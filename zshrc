
# ZSH_THEME="robbyrussell"
ZSH_THEME="bira"

plugins=(
colored-man-pages
copydir
copyfile
dircycle
git
zsh-syntax-highlighting
zsh-autosuggestions
zsh-interactive-cd
z
)

# vim mode
bindkey -v
export KEYTIMEOUT=1

autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

source $ZSH/oh-my-zsh.sh

# dircycle rebind to ALT+arrow
bindkey '^[[1;3D' insert-cycledleft
bindkey '^[[1;3C' insert-cycledright

case $(uname -a) in
   *Microsoft*) unsetopt BG_NICE ;;
esac

function preexec() {
  timer=${timer:-$SECONDS}
}

function precmd() {
  if [ $timer ]; then
    timer_show=$(($SECONDS - $timer))
    export RPROMPT="%F{cyan}${timer_show}s %{$reset_color%}"
    unset timer
  fi
}


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
